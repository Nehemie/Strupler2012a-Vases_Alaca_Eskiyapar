# README

This is the text I submitted for publication in Anatolia Antiqua XX (2012)

## Text

I have reformatted the pre-print version of the article written in AsciiDoc and
optimized for [Asciidoctor](http://asciidoctor.org). It choose this format here
because I wanted to try something different than [R|Common]Markdown. It is also
compiled with the gem
[asciidoctor-bibtex](https://github.com/asciidoctor/asciidoctor-bibtex)


    asciidoctor -r  asciidoctor-bibtex Strupler2012-RVAE.adoc

I use asciidoctor and 

## Images

Whenever possible I provided a vector file (.svg)

## License

Néhémie Strupler CC BY-SA
